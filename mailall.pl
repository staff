#!/usr/bin/perl -w
use strict;

my $data_dir = shift @ARGV;

my $n = 0;
my %address;

foreach my $file ( glob("$data_dir/*") ) {
  open(FILE,"<$file") or die $!;
  while (<FILE>) {
    /\b(\w[\w\-\.\+]*\@(([\w\.\-]+\.)+\w+))\b/ or warn "can't parse line: $_\n";
    $address{lc($1)}=1;
    $n++;
  }
}

print scalar(keys(%address)), " ($n)\n";
print join(",",keys %address),"\n";

